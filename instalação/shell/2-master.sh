#!/bin/bash
# Somente no master
echo AGORA SOMENTE NO NODE MASTER
echo INICIANDO O CLUSTER
sleep 3

kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address $(hostname -i)

mkdir -p $HOME/.kube

sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Para visualizar o Token do kubedm acaso voce nao tenha anotado ele

# kubeadm token create --print-join-command