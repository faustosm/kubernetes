#!/bin/bash

## Em todos os servidores

echo ADICIONANDO REPOSITORIO KUBERNETES

sleep 3

cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

echo ATUALIZANDO REPOSITORIO

sleep 3

yum update -y

echo DESABILITANDO SWAP E FIREWALL

swapoff -a

setenforce 0

systemctl stop firewalld

systemctl disable firewalld

echo INSTALANDO KUBEADM, KUBECTL KUBELET

sleep 3

yum install -y kubelet kubeadm kubectl && systemctl enable kubelet && systemctl start kubelet

systemctl start docker && systemctl enable docker && systemctl start kubelet && systemctl enable kubelet

# Ainda na família do Red Hat, é importante configurar alguns parâmetros de kernel no sysctl:

echo CRIA O ARQUIVO K8S.CONF

touch /etc/sysctl.d/k8s.conf

echo "net.bridge.bridge-nf-call-ip6tables" = 1 >> /etc/sysctl.d/k8s.conf

echo "net.bridge.bridge-nf-call-iptables" = 1 >> /etc/sysctl.d/k8s.conf

docker info | grep -i cgroup

sed -i "s/cgroup-driver=systemd/cgroup-driver=cgroupfs/g" /usr/lib/systemd/system/kubelet.service.d/10-kubeadm.conf

# ou

# sed -i "s/cgroup-driver=systemd/cgroup-driver=cgroupfs/g" /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

systemctl daemon-reload && systemctl restart kubelet